const employee = require("./model")

exports.createEmp =async (req,res)=>{
    console.log("adding employee")   
    try{
      const emp = new employee(req.body)
      const createEmp= await emp.save();
      res.status(201).send(createEmp)    
    }
    catch(err)
    {
      res.status(500).send(err)
    }     
 }
 exports.readEmpData =async (req,res)=>{
  console.log("getting all employees")   
 try{
     const allEmp = await employee.find();
    res.status(200).send(allEmp)  
  }
  catch(err)
  {
    res.status(500).send(err)
  }  
      
}

exports.getParticularEmp =async (req,res)=>{
  console.log("getting employee of given id")   
 try{
     const _id=  req.params.id;
     const allEmp = await employee.findById(_id);
    res.status(200).send(allEmp)  
  }
  catch(err)
  {
    res.status(500).send(err)
  }  
      
}


exports.updateEmpDet =async (req,res)=>{
  console.log("updating details of employee with given id")   
 try{
     const _id=  req.params.id;
     const updatedEmp = await employee.findByIdAndUpdate(_id,req.body,{new:true});
    res.status(200).send(updatedEmp)  
  }
  catch(err)
  {
    res.status(500).send(err)
  }  
      
}

exports.deleteEmp =async (req,res)=>{
  console.log("updating details of employee with given id")   
 try{
     const _id=  req.params.id;
     const updatedEmp = await employee.findByIdAndDelete(_id);
    res.status(200).send(updatedEmp)  
  }
  catch(err)
  {
    res.status(500).send(err)
  }  
      
}