const express = require("express");
const router = express.Router()
const employee = require("./model")
const{createEmp,readEmpData,getParticularEmp,updateEmpDet,deleteEmp}=require("./constroller");


router.post("/addEmp",createEmp)

router.get("/getEmp",readEmpData)

router.get("/getEmp/:id",getParticularEmp)

router.patch("/updateEmp/:id",updateEmpDet)

router.delete("/deleteEmp/:id",deleteEmp)
module.exports=router