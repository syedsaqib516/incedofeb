const express = require("express");
const router = express.Router()
const employee = require("./model")
const{createEmp,readEmpData,getParticularEmp,updateEmpDet,deleteEmp}=require("./constroller");


router.post("/addTask",createEmp)

router.get("/getTask",readEmpData)

router.get("/getTask/:id",getParticularEmp)

router.patch("/updateTask/:id",updateEmpDet)

router.delete("/deleteDelete/:id",deleteEmp)
module.exports=router