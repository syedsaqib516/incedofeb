const { all } = require(".");
const tasks = require("./model")
const fetch = require("isomorphic-fetch")

exports.createEmp =async (req,res)=>{
    console.log("adding task")   
    try{
      const task = new tasks(req.body)
      const createTask= await task.save();
      res.status(201).send(createTask)    
    }
    catch(err)
    {
      res.status(500).send(err)
    }     
 }
 exports.readEmpData =async (req,res)=>{
  console.log("getting all tasks")   
 try{
     const allTasks = await tasks.find();
    res.status(200).send(allTasks)  
  }
  catch(err)
  {
    res.status(500).send(err)
  }  
      
}

exports.getParticularEmp =async (req,res)=>{
  console.log("getting task for employee of given id")   
 try{
     const _id=  req.params.id;
     const task = await tasks.findById(_id);
     
    res.status(200).json(task)  
  }
  catch(err)
  {
    res.status(500).send(err)
  }  
      
}


exports.updateEmpDet =async (req,res)=>{
  console.log("updating details of employee with given id")   
 try{
     const _id=  req.params.id;
     const updatedEmp = await employee.findByIdAndUpdate(_id,req.body,{new:true});
    res.status(200).send(updatedEmp)  
  }
  catch(err)
  {
    res.status(500).send(err)
  }  
      
}

exports.deleteEmp =async (req,res)=>{
  console.log("updating details of employee with given id")   
 try{
     const _id=  req.params.id;
     const updatedEmp = await employee.findByIdAndDelete(_id);
    res.status(200).send(updatedEmp)  
  }
  catch(err)
  {
    res.status(500).send(err)
  }  
      
}