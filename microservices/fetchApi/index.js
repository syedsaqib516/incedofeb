const fetch=require("isomorphic-fetch")

async function performCRUDOperations() {
    try {
      const posts = await fetch('https://jsonplaceholder.typicode.com/posts');
      if (posts.ok) {
        const postsData = await posts.json();
        console.log(postsData);
      } else {
        throw new Error(`Error retrieving posts: ${posts.statusText}`);
      }
  
      const post = await fetch('https://jsonplaceholder.typicode.com/posts/1');
      if (post.ok) {
        const postData = await post.json();
        console.log(postData);
      } else {
        throw new Error(`Error retrieving post: ${post.statusText}`);
      }
  
      const newPost = {
        title: 'My new post',
        body: 'This is the content of my new post',
        userId: 1,
      };
      const createPost = await fetch('https://jsonplaceholder.typicode.com/posts', {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify(newPost),
      });
      if (createPost.ok) {
        const createPostData = await createPost.json();
        console.log(`New post created with ID ${createPostData.id}`);
      } else {
        throw new Error(`Error creating post: ${createPost.statusText}`);
      }
  
      const updatedPost = {
        title: 'Updated post title',
        body: 'This is the updated content of the post',
      };
      const updatePost = await fetch('https://jsonplaceholder.typicode.com/posts/1', {
        method: 'PUT',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify(updatedPost),
      });
      if (updatePost.ok) {
        const updatedPostData = await updatePost.json();
        console.log(`Post updated: ${updatedPostData}`);
      } else {
        throw new Error(`Error updating post: ${updatePost.statusText}`);
      }
  
      const deletePost = await fetch('https://jsonplaceholder.typicode.com/posts/1', {
        method: 'DELETE',
      });
      if (deletePost.ok) {
        console.log('Post deleted');
      } else {
        throw new Error(`Error deleting post: ${deletePost.statusText}`);
      }
  
      const response = {
        message: 'CRUD operations performed successfully',
      };
  
      return response;
    } catch (error) {
      console.error(error);
      const response = {
        error: error.message,
      };
      return response;
    }
  }
  
  performCRUDOperations().then(response => {
    console.log(response);
  });
  