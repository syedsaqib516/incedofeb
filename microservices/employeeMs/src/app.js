const express = require("express");
const http = require("http");
require("./db/connection")
const router =require("./api/emp/index")
const employee = require("./api/emp/model")
const appName='employeeMs';
const port= process.env.PORT||9000;
const eurekaHelper= require('./eureka-helper');
eurekaHelper.registerWithEureka("employeeMs",port)

const app=express();

const server=http.createServer(app)


//const ip= process.env.IP || "0.0.0.0";

app.use(express.json())
app.use(router);

server.listen(port,()=>{
   console.log("express server listening on http://localhost:%d",port);
});


