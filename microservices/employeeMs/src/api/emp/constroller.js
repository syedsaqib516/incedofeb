const { all } = require(".");
const employee = require("./model")
const fetch = require("isomorphic-fetch")

exports.createEmp =async (req,res)=>{
    console.log("adding employee")   
    try{
      const emp = new employee(req.body)
      const createEmp= await emp.save();
      res.status(201).send(createEmp)    
    }
    catch(err)
    {
      res.status(500).send(err)
    }     
 }
 exports.readEmpData =async (req,res)=>{
  console.log("getting all employees")   
 try{
     const allEmp = await employee.find();
    res.status(200).send(allEmp)  
  }
  catch(err)
  {
    res.status(500).send(err)
  }  
      
}

exports.getParticularEmp =async (req,res)=>{
  console.log("getting employee of given id")   
 try{
     const _id=  req.params.id;
     const emp= await employee.findById(_id);
       const task = await fetch(`http://localhost:9001/getTask/63edcb087183555686d3c11a`);
    
     const taskData = await task.json();

      const post = await fetch('https://jsonplaceholder.typicode.com/posts/1');
    
      const postData = await post.json();
      console.log(postData);
       
      
        const newData = {...emp,...taskData,...postData}
    res.status(200).json(newData)  
  }
  catch(err)
  {
    res.status(500).send(err)
  }  
      
}


exports.updateEmpDet =async (req,res)=>{
  console.log("updating details of employee with given id")   
 try{
     const _id=  req.params.id;
     const updatedEmp = await employee.findByIdAndUpdate(_id,req.body,{new:true});
    res.status(200).send(updatedEmp)  
  }
  catch(err)
  {
    res.status(500).send(err)
  }  
      
}

exports.deleteEmp =async (req,res)=>{
  console.log("updating details of employee with given id")   
 try{
     const _id=  req.params.id;
     const updatedEmp = await employee.findByIdAndDelete(_id);
    res.status(200).send(updatedEmp)  
  }
  catch(err)
  {
    res.status(500).send(err)
  }  
      
}