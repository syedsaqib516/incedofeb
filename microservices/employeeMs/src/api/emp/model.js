const mongoose=require("mongoose");

const empSchema= new mongoose.Schema({
empId:{type:Number,maxlength:3,unique:true,required:true},
empName:{type:String,maxlength:128,required:true},
empPhone:{type:Number,maxlength:10,unique:true,required:true,match:/d{10}/}

})  
 const employee= new mongoose.model("EmpCollection",empSchema);
 module.exports = employee